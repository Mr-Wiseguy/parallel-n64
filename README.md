# ParallelN64 (Parallel Launcher Edition)

A fork of [ParallelN64](https://git.libretro.com/libretro/parallel-n64) that adds the GLideN64 graphics plugin and some additional settings and features. Designed to be used with Parallel Launcher.

### Platform Builds

Build scripts are found [here](https://gitlab.com/parallel-launcher/parallel-n64/-/tree/master/scripts).
